﻿using PFR_Chervov.DbModel;
using PFR_Chervov.Helpers;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace PFR_Chervov.Pages
{
    /// <summary>
    /// Логика взаимодействия для departmentsPage.xaml
    /// </summary>
    public partial class departmentsPage : Page
    {
        DbSet<Отделы> departmentsDb;

        public departmentsPage()
        {
            InitializeComponent();

            PageHelper.PageName.Text = "Список отделов ПФР";

            departmentsDb = PageHelper.ConnectDb.Отделы;
            lvDepartments.ItemsSource = departmentsDb.ToList();
        }
    }
}
