﻿using PFR_Chervov.Helpers;
using PFR_Chervov.Pages;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace PFR_Chervov
{
    /// <summary>
    /// Логика взаимодействия для MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();

            PageHelper.MainFrame = mainFrame;
            PageHelper.PageName = tbPageName;

            PageHelper.MainFrame.Navigate(new mainPage());
        }

        private void btnDepartments_Click(object sender, RoutedEventArgs e)
        {
            PageHelper.MainFrame.Navigate(new departmentsPage());
        }

        private void btnEmployees_Click(object sender, RoutedEventArgs e)
        {
            PageHelper.MainFrame.Navigate(new employeesPage());
        }

        private void btnClients_Click(object sender, RoutedEventArgs e)
        {
            PageHelper.MainFrame.Navigate(new clientsPage());
        }

        private void btnBack_Click(object sender, RoutedEventArgs e)
        {
            PageHelper.MainFrame.Navigate(new mainPage());
        }
    }
}
