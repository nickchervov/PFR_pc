//------------------------------------------------------------------------------
// <auto-generated>
//     Этот код создан по шаблону.
//
//     Изменения, вносимые в этот файл вручную, могут привести к непредвиденной работе приложения.
//     Изменения, вносимые в этот файл вручную, будут перезаписаны при повторном создании кода.
// </auto-generated>
//------------------------------------------------------------------------------

namespace PFR_Chervov.DbModel
{
    using System;
    using System.Collections.Generic;
    
    public partial class Отделы
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public Отделы()
        {
            this.Данные_клиента = new HashSet<Данные_клиента>();
            this.Сотрудники_ПФР = new HashSet<Сотрудники_ПФР>();
        }
    
        public int Код_отдела { get; set; }
        public string Название_отдела { get; set; }
    
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Данные_клиента> Данные_клиента { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Сотрудники_ПФР> Сотрудники_ПФР { get; set; }
    }
}
