//------------------------------------------------------------------------------
// <auto-generated>
//     Этот код создан по шаблону.
//
//     Изменения, вносимые в этот файл вручную, могут привести к непредвиденной работе приложения.
//     Изменения, вносимые в этот файл вручную, будут перезаписаны при повторном создании кода.
// </auto-generated>
//------------------------------------------------------------------------------

namespace PFR_Chervov.DbModel
{
    using System;
    using System.Collections.Generic;
    
    public partial class Накопительная_часть
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public Накопительная_часть()
        {
            this.Доходы = new HashSet<Доходы>();
        }
    
        public int Код_процентной_ставки { get; set; }
        public string Процентная_ставка { get; set; }
    
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Доходы> Доходы { get; set; }
    }
}
